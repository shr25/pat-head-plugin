### 介绍

基于shr25-qq-robot项目的自定义插件

主项目地址: https://gitee.com/shr25/shr25-qq-robot 
demo项目地址: https://gitee.com/shr25/shr25-qq-robot-demo-plugin  
安装文档在项目的wiki中有  
需要本地有java环境  
数据源使用的mysql8.x(生产)或sqlite（本地） 
使用mysql的，初始化sql文件在主项目地址中有,可自行下载 